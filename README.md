# Alt JS Examples

This is documentation over the multiple altJS languages that exist.
It shows how each language can solve a few simple problems in both OO paradigm and FP paradigm. It may even include a few examples of creating a small application. Ideally, each languages implementations would accentuate the features of the given language in a way that show the reader how each of them are you unique.
