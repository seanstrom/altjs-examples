# Pair implementation without Prototype
Pair = (x, y) ->
  fst: -> x
  snd: -> y
  swap: -> Pair y, x

# Pair implementation with Prototype
class Pair
  constructor: (x, y) ->
    return new Pair x, y unless this instanceof Pair
    @pair = [x, y]

  fst: -> @pair[0]
  snd: -> @pair[1]
  swap: -> Pair @pair[1], @pair[0]
