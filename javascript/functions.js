// Name Function
function f() {}

// Variable Assigned Anonymous Function
var f = function() {}

// Calling a Function
f()

// `this` in Functions
function f() {
  return this
}

// Injecting `this` into Functions
thisObj = {name: 'foo'}
f.call(thisObj)

// Injecting `this` into Functions with arguments
arg = 1
thisObj = {name: 'foo'}
f.call(thisObj, arg)

// Injecting `this` into Functions with a list of arguments
args = [1, 2, 3]
thisObj = {name: 'foo'}
f.apply(thisObj, args)

// Binding a Function's `this` value without invoking the Function
thisObj = {name: 'foo'}
f.bind(thisObj)

// Binding a Function's `this` value and arguments without invoking the Function
arg = 1
thisObj = {name: 'foo'}
f.bind(thisObj, arg)

// Binding a Functions's arguments without invocations - Partial Application
arg = 1
f.bind(null, arg)
