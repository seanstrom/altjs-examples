// Pair implementation without Prototype
function Pair(x, y) {
  return {
    fst: function() { return x },
    snd: function() { return y }
    swap: function() { return Pair(y, x) }
  }
}

// Pair implementation with Prototype
function Pair(x, y) {
  if (!this instanceof Pair)
    return new Pair(x, y)

  this.pair = [x, y]
}

Pair.prototype.fst = function() {
  return this.pair[0]
}

Pair.prototype.snd = function() {
  return this.pair[1]
}

Pair.prototype.swap = function() {
  return Pair(this.snd(), this.fst())
}
