# Global Scope
x = 0

f = do ->
  # Function Scope
  x = 1

# Global Scope
x # 1

console.log x
